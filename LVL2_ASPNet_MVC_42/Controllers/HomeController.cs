﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_42.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CodingIDMessage()
        {
            ViewBag.Message = "Learn, Code, Share";

            return View();
        }

        public ActionResult JegErs()
        {
            ViewBag.Message = "Jeges Electronic Reporting System";

            return View();
        }
    }
}