﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LVL2_ASPNet_MVC_42
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CodingId",
                url: "CodingId/CodingIDMessage",
                defaults: new { controller = "Home", action = "CodingIDMessage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Jegers",
                url: "CodingId/JegErs",
                defaults: new { controller = "Home", action = "JegErs", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
